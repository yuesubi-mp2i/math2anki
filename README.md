# Math2Anki
A python script to make Anki decks of my math classes.


## Usage
```sh
python math2anki.py make <math_notes> [-o output_dir] [-n name]
# python math2anki.py install <output_dir> [anki_media_folder]
```


## Process
* extractions of *blocks*
* creation of an individual file for each the block
* compilation to svg of each block
* creation of a csv


## Bugs
* fails to extract a variable defined with `let` by mistaking
  it with a function definition


## Dependencies
- `typst`
- `anki`
