import argparse

from .make import MakeArgs


def config_make_parser(parser: argparse.ArgumentParser):
    parser.add_argument(
        "math_notes",
        type=str,
        help="typst math notes file"
    )
    parser.add_argument(
        "-o",
        type=str,
        default="deck",
        required=False,
        help="output directory",
        metavar="out_dir"
    )
    parser.add_argument(
        "-n",
        type=str,
        default="deck",
        required=False,
        help="name of the created deck",
        metavar="name"
    )
    parser.add_argument(
        "-i",
        type=str,
        default="",
        required=False,
        help="file to include",
        metavar="include"
    )
    parser.add_argument(
        "-c",
        default=False,
        action="store_true",
        help="compatibility hashes",
    )


def config_install_parser(parser: argparse.ArgumentParser):
    pass


def parse_program_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        prog="math2anki.py",
        description="Generate anki flashcards from yuesubi's math notes."
    )
    sub_parser_action = parser.add_subparsers(title="subcommands")

    make_parser = sub_parser_action.add_parser(
        "make",
        help="Generates a folder with the media and cards inside."
    )
    config_make_parser(make_parser)

    install_parser = sub_parser_action.add_parser("install")

    return parser.parse_args()


def create_make_args(args: argparse.Namespace) -> MakeArgs:
    assert "math_notes" in args
    assert "o" in args
    assert "n" in args
    assert "i" in args
    assert "c" in args

    make_args = MakeArgs()
    make_args.typ_source = args.math_notes
    make_args.out_dir = args.o
    make_args.name = args.n
    make_args.include = args.i
    make_args.hash_with_kind = not args.c

    return make_args
