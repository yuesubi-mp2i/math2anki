import os
import shutil

from .args import MakeArgs
from .block import Block
from .deck import Card, Deck
from .extractor import extract_blocks_and_header
from .typst import compile_typst, write_to_typst


ANKI_TEMPLATE = os.path.join(os.path.dirname(__file__), "..", "..", "templates",
    "anki.typ")
TMP = os.path.join("/tmp", "math2anki")


def create_card_media(block: Block, header: str,
        front_svg: str, back_svg: str):
    temp_typst_file = os.path.join(TMP, "temp.typ")

    write_to_typst(header + f"\n{block.kind} : {block.name}", temp_typst_file)
    compile_typst(temp_typst_file, front_svg)

    write_to_typst(header + '\n' + block.content, temp_typst_file)
    compile_typst(temp_typst_file, back_svg)


def create_deck(make_args: MakeArgs) -> Deck:
    if not os.path.exists(make_args.typ_source):
        raise FileNotFoundError()
    if os.path.exists(make_args.out_dir):
        raise FileExistsError()
    
    os.makedirs(TMP, exist_ok=True)
    shutil.copy(ANKI_TEMPLATE, os.path.join(TMP, "anki.typ"))
    header_name = os.path.basename(make_args.include)

    if len(header_name) > 0:
        header_path = os.path.join(TMP, header_name)
        if os.path.exists(header_path):
            os.remove(header_path)
        shutil.copy(
            make_args.include,
            os.path.join(TMP, header_name)
        )
    
    blocks, header = extract_blocks_and_header(
        make_args.typ_source,
        make_args.hash_with_kind
    )
    if len(header_name) > 0:
        header = f"#import \"{header_name}\": *\n" + header

    deck = Deck()

    media_dir = os.path.join(make_args.out_dir, "media")
    os.makedirs(media_dir, exist_ok=True)

    for block in blocks:
        front_svg_file = os.path.join(media_dir,
            f"{make_args.name}_{block.hash_}_f.svg")
        back_svg_file = os.path.join(media_dir,
            f"{make_args.name}_{block.back_hash}_b.svg")
        create_card_media(block, header, front_svg_file, back_svg_file)

        deck.media.append(front_svg_file)
        deck.media.append(back_svg_file)

        deck.cards.append(Card(
            f"<img src={os.path.basename(front_svg_file)} width=100%>",
            f"<img src={os.path.basename(back_svg_file)} width=100%>"
        ))

    return deck


def make_main(make_args: MakeArgs):
    deck = create_deck(make_args)
    csv_file = os.path.join(make_args.out_dir, "deck.csv")
    deck.save_as_csv(csv_file)
