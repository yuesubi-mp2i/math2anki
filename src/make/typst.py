import os


def write_to_typst(content: str, out_file: str):
    with open(out_file, "w+") as file:
        file.writelines([
            """#import "anki.typ" : yanki_template\n""",
            """#show: doc => yanki_template[#doc]\n""",
        ])
        file.write(content)


def compile_typst(source: str, output: str):
    os.system(f"typst compile {source} {output}")