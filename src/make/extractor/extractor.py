import hashlib
import uuid
import sys
from typing import Any, Iterable, Iterator

from ..block import Block
from .lexer import Lexer
from .parser import symbols_to_str, Parser, Symbol, FuncCall, FuncDef


BLOCK_TYPES = {
    "ydef": "Définition",
    "ynot": "Notation",
    "ytheo": "Théorème",
    "yprop": "Propriété",
    "ylemme": "Lemme",
    "ymetho": "Méthode",
    "ycor": "Corollaire",
    "yalgo": "Algorithme"
}

PROVABLE_BLOCKS = ["ytheo", "yprop", "ylemme", "ymetho", "ycor", "yalgo"]
PROOF_BLOCK = "yproof"


def is_instance_of_any(object_: Any, types: list[type]) -> bool:
    return any(isinstance(object_, t) for t in types)


def find_symbols_of_types(
        symbol_types: list[type(Symbol)],
        symbols: Iterable[Symbol]
    ) -> list[FuncDef]:

    found = []

    for symbol in symbols:
        if is_instance_of_any(symbol, symbol_types):
            found.append(symbol)
        
        if isinstance(symbol, FuncCall):
            for value in symbol.kwargs.values():
                found.extend(find_symbols_of_types(symbol_types, value))
            for arg in symbol.args:
                found.extend(find_symbols_of_types(symbol_types, arg))
            found.extend(find_symbols_of_types(symbol_types, symbol.content))

        elif isinstance(symbol, FuncDef):
            for _, value in symbol.args:
                if value is not None:
                    found.extend(find_symbols_of_types(symbol_types, value))
            found.extend(find_symbols_of_types(symbol_types, symbol.content))
        
    return found


def create_blocks(
        func_calls: list[FuncCall],
        hash_with_kind: bool = True
    ) -> list[Block]:

    blocks = []
    provable = None

    for func_call in func_calls:
        if func_call.name in BLOCK_TYPES:
            block = Block(
                hash_="",
                back_hash="",
                kind=BLOCK_TYPES.get(func_call.name, "Unknown"),
                name="",
                content=symbols_to_str(func_call.content)
            )

            if "name" in func_call.kwargs.keys():
                block.name = symbols_to_str(func_call.kwargs["name"]).strip(" []")
            else:
                print("No name found for : " + block.content)
                block.name = str(uuid.uuid4())
                sys.exit(1)
            
            sha256 = hashlib.new("sha256")
            hash_input = block.name.encode()
            if hash_with_kind:
                hash_input += block.kind.encode()
            sha256.update(hash_input)
            block.hash_ = sha256.hexdigest()[:24]

            sha256 = hashlib.new("sha256")
            sha256.update(block.content.encode())
            block.back_hash = sha256.hexdigest()[:24]

            blocks.append(block)

            if func_call.name in PROVABLE_BLOCKS:
                provable = block

        elif func_call.name == PROOF_BLOCK and provable is not None:
            provable.content += "\n\n== #underline[Preuve] :\n"
            provable.content += symbols_to_str(func_call.content)

            sha256 = hashlib.new("sha256")
            sha256.update(provable.content.encode())
            provable.back_hash = sha256.hexdigest()[:24]

    return blocks


def extract_blocks_and_header(
        file_path: str,
        hash_with_kind: bool = True
    ) -> tuple[list[Block], str]:

    symbols = []

    with open(file_path, "r") as file:
        content = [[char for char in line] for line in file]

        lexer = Lexer(content)
        lexer.lex()
        parser = Parser(lexer.tokens)
        parser.parse()
        symbols = parser.symbols
    
    func_calls = find_symbols_of_types([FuncCall], symbols)
    blocks = create_blocks(func_calls, hash_with_kind)

    func_defs = find_symbols_of_types([FuncDef], symbols)
    header = '\n'.join(map(symbols_to_str, func_defs))

    return blocks, header
