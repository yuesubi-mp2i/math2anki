from dataclasses import dataclass
from typing import Union

from .lexer import EmptyToken, Token, Word

Content = list['Symbol']


@dataclass
class FuncCall:
    name: str
    kwargs: dict[str, Content]
    args: list[Content]
    content: Content


@dataclass
class FuncDef:
    name: str
    args: list[tuple[str, Union[Content, None]]]
    content: Content


Symbol = Union[FuncCall, FuncDef, Content, str]


SCOPES = {
    EmptyToken.LBRACE: EmptyToken.RBRACE,
    EmptyToken.LBRACK: EmptyToken.RBRACK,
    EmptyToken.LPAREN: EmptyToken.RPAREN
}


class Parser:
    def __init__(self, tokens: list[Token]):
        self.tokens: list[Token] = tokens
        self.caret: int = 0
        self.symbols: Symbol = []

        self.to_next_token()
    
    def to_next_token(self):
        self.caret += 1
    
    @property
    def token(self) -> Union[Token, None]:
        return (self.tokens[self.caret] if self.caret < len(self.tokens)
            else None)

    def foresee_token(self, additional_steps: int) -> Union[Token, None]:
        return (self.tokens[self.caret + additional_steps]
            if self.caret + additional_steps < len(self.tokens) else None)

    def assert_msg(self, msg: str) -> str:
        tok = self.token
        return f"At {tok.loc.line}::{tok.loc.column} on '{tok.content}' : {msg}"

    def parse(self):
        self.symbols = self.extract_content([])
    
    def extract_content(self, end_tokens: list[Token]) -> Content:
        cont = []
        scopes = []

        is_in_math = False

        while (self.token is not None and
                (is_in_math or self.token.content not in end_tokens or len(scopes) > 0)):

            if self.token.content == EmptyToken.DOLLAR:
                is_in_math = not is_in_math
            elif not is_in_math:
                if self.token.content == EmptyToken.HASHTAG:
                    cont.append(self.extract_func())
                    continue
                elif (not isinstance(self.token.content, Word) and
                        self.token.content in SCOPES.keys()):
                    scopes.append(self.token.content)
                elif (not isinstance(self.token.content, Word) and
                            self.token.content in SCOPES.values()):
                    assert len(scopes) > 0, \
                        self.assert_msg("scope never opened")
                    assert self.token.content == SCOPES[scopes[-1]], \
                        self.assert_msg(f"expected {SCOPES[scopes[-1]]} instead")
                    scopes.pop()
            
            if isinstance(self.token.content, Word):
                cont.append(self.token.content.text)
            else:
                cont.append(self.token.content.value)
            self.to_next_token()
        
        return cont
    
    def extract_func(self) -> Union[FuncCall, FuncDef, str]:
        assert self.token.content == EmptyToken.HASHTAG, \
            self.assert_msg(f"expected a '#'")
        self.to_next_token()

        if not isinstance(self.token.content, Word):
            raise SyntaxError(self.assert_msg(f"Expected a word after '#'"))

        words = list(filter(lambda word: word != ' ',
            self.token.content.text.split(' ')))
        if words[0] == "let":

            return self.extract_func_def()
        else:
            caret = self.caret
            try:
                return self.extract_func_call()
            except AssertionError as e:
                print(f"Failed to extract function: {e.args}")
                self.caret = caret
                return EmptyToken.HASHTAG.value

    def extract_func_def(self) -> FuncDef:
        words = list(filter(lambda word: word != ' ',
            self.token.content.text.split(' ')))
        assert words[0] == "let", self.assert_msg("expected 'let'")
        assert len(words) == 2, self.assert_msg(f"expected only a word after 'let', got {len(words)}")

        func_def = FuncDef(words[1], [], [])
        
        self.to_next_token()
        assert self.token.content == EmptyToken.LPAREN, \
            self.assert_msg("expected '('")
        self.to_next_token()

        while (self.token.content is not None and
                self.token.content != EmptyToken.RPAREN):
            assert isinstance(self.token.content, Word), \
                self.assert_msg("expected a word")
            arg_name = self.token.content.text.strip(" \n\t")

            self.to_next_token()
            assert self.token.content in (EmptyToken.COLUMN, EmptyToken.COMMA,
                EmptyToken.RPAREN), self.assert_msg("expected ',', '.' or ')'")

            if self.token.content == EmptyToken.COLUMN:
                arg_default = self.extract_content([EmptyToken.COMMA,
                    EmptyToken.RPAREN])
                func_def.args.append((arg_name, arg_default))
            else:
                func_def.args.append((arg_name, None))
            
            if self.token.content == EmptyToken.COMMA:
                self.to_next_token()
            else:
                assert self.token.content == EmptyToken.RPAREN, \
                    self.assert_msg("expected ')'")

        self.to_next_token()
        self.consume_blanks()

        assert self.token.content == EmptyToken.EQUAL, \
            self.assert_msg("expected '='")
        self.to_next_token()
        self.consume_blanks()

        assert self.token.content == EmptyToken.LBRACE, \
            self.assert_msg("expected '{'")
        self.to_next_token()
        func_def.content = self.extract_content([EmptyToken.RBRACE])

        assert self.token.content == EmptyToken.RBRACE, \
            self.assert_msg("expected '}'")
        self.to_next_token()

        return func_def

    def extract_func_call(self) -> FuncCall:
        words = list(filter(lambda word: word != ' ',
            self.token.content.text.split(' ')))
        assert len(words) == 1, self.assert_msg(f"Expected a word, got {words}")

        func_call = FuncCall(words[0], {}, [], [])
        is_func = False
        
        self.to_next_token()
        if self.token.content == EmptyToken.LPAREN:
            is_func = True
            self.to_next_token()

            while (self.token.content is not None and
                    self.token.content != EmptyToken.RPAREN):
                arg_name = None
                
                if self.foresee_token(1).content == EmptyToken.COLUMN:
                    assert isinstance(self.token.content, Word), \
                        self.assert_msg(f"Expected a kwarg name")
                    arg_name = self.token.content.text.strip(" \n\t")
                    self.to_next_token()

                    assert self.token.content == EmptyToken.COLUMN, \
                        self.assert_msg(f"Expected a ':'")
                    self.to_next_token()

                value = self.extract_content([EmptyToken.COMMA, EmptyToken.RPAREN])

                if arg_name is None:
                    func_call.args.append(value)
                else:
                    func_call.kwargs[arg_name] = value
                
                if self.token.content == EmptyToken.COMMA:
                    self.to_next_token()
                else:
                    assert self.token.content == EmptyToken.RPAREN, \
                        self.assert_msg(f"Expected a ')'")

            self.to_next_token()
            self.consume_blanks()

        if self.token.content == EmptyToken.LBRACK:
            is_func = True
            self.to_next_token()
            func_call.content = self.extract_content([EmptyToken.RBRACK])

            assert self.token.content == EmptyToken.RBRACK, \
                self.assert_msg("Expected a ']'")
            self.to_next_token()
        
        assert is_func, "Misunderstood for a function"

        return func_call

    def consume_blanks(self):
        while self.token.content is not None and isinstance(self.token.content, Word):
            if len(self.token.content.text.strip(" \n\t")) > 0:
                break
            self.to_next_token()


def symbols_to_str(symbol: Symbol) -> str:
    text = ""

    if isinstance(symbol, FuncCall):
        text += f"#{symbol.name}("
        text += ", ".join(
            [name + ":" + symbols_to_str(value)
                for name, value in symbol.kwargs.items()] +
            list(map(symbols_to_str, symbol.args))
        )
        text += ')'

        content_str = symbols_to_str(symbol.content)
        if len(content_str.strip()) > 0:
            text += "[" + symbols_to_str(symbol.content) + "]"

    elif isinstance(symbol, FuncDef):
        text += f"#let {symbol.name}("
        text += ", ".join([
            (name if default is None else f"{name}: {symbols_to_str(default)}")
            for name, default in symbol.args
        ])
        text += ") = {" + symbols_to_str(symbol.content) + "}"

    elif isinstance(symbol, list):
        for sub_symbol in symbol:
            text += symbols_to_str(sub_symbol)

    elif isinstance(symbol, str):
        text += symbol

    else:
        assert False
    
    return text
