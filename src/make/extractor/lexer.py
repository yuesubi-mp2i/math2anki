import dataclasses
import enum

from typing import Iterable, Iterator, Union


class EmptyToken(enum.Enum):
    LPAREN = '('
    RPAREN = ')'
    LBRACK = '['
    RBRACK = ']'
    LBRACE = '{'
    RBRACE = '}'
    EQUAL = '='
    COLUMN = ':'
    HASHTAG = '#'
    COMMA = ','
    DOLLAR = '$'


@dataclasses.dataclass
class Word:
    text: str


@dataclasses.dataclass
class Location:
    line: int
    column: int


@dataclasses.dataclass
class Token:
    content: Union[Word, EmptyToken]
    loc: Location


class Lexer:
    def __init__(self, lines: Iterable[Iterable[str]]):
        self.lines: Iterator[Iterable[str]] = iter(lines)
        self.line: Union[Iterator[str], None] = None
        
        self.loc: Location = Location(line=0, column=0)
        self.char: Union[str, None] = None

        self.tokens: list[Token] = []

        self.to_next_line()
        self.move_caret()
    
    def to_next_line(self):
        iterable = next(self.lines, None)
        self.line = None if iterable is None else iter(iterable)

        self.loc.line += 1
        self.loc.column = 0
    
    def move_caret(self):
        if self.line is not None:
            self.char = next(self.line, None)
            self.loc.column += 1

            if self.char is None:
                self.to_next_line()
                self.move_caret()

    def extract_word(self) -> Word:
        word = Word("")
        next_is_escaped = False

        while self.char is not None:
            try:
                if next_is_escaped:
                    next_is_escaped = False
                    raise ValueError("This character is escaped")

                EmptyToken(self.char)
                return word
            except ValueError:
                word.text += self.char
                if self.char == '\\':
                    next_is_escaped = True

            self.move_caret()
        
        return word

    def lex(self):
        while self.char is not None:
            try:
                self.tokens.append(Token(
                    EmptyToken(self.char),
                    dataclasses.replace(self.loc)
                ))
                self.move_caret()

            except ValueError:
                self.tokens.append(Token(
                    self.extract_word(),
                    dataclasses.replace(self.loc)
                ))
