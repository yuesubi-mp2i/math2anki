class MakeArgs:
    def __init__(self):
        self.typ_source: str = ""
        self.out_dir: str = ""
        self.name: str = ""
        self.include: str = ""
        self.hash_with_kind: bool = True