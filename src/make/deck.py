import csv


class Card:
    def __init__(self, front: str, back: str):
        self.front: str = front
        self.back: str = back
    
    def as_list(self) -> list[str]:
        return [self.front, self.back]


class Deck:
    def __init__(self):
        self.cards: list[Card] = []
        self.media: list[str] = []
    
    def save_as_csv(self, file_path: str):
        with open(file_path, 'w+') as csv_file:
            writer = csv.writer(csv_file, delimiter=',')
            writer.writerows(map(lambda card: card.as_list(), self.cards))