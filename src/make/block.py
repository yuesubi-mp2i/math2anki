from dataclasses import dataclass

@dataclass
class Block:
    hash_: str
    back_hash: str
    kind: str
    name: str
    content: str