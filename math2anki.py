#!/usr/bin/python3
from src import *


def main():
    args = parse_program_args()

    if "math_notes" in args:
        make_args = create_make_args(args)
        make_main(make_args)
    else:
        raise NotImplementedError()


if __name__ == "__main__":
    main()