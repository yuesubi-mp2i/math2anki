#import "math_layout.typ": yconf_math
#let yanki_template(doc) = {
    set text(
        // size: 62pt,
        fill: rgb("ffffff")
    )

    set page(
        width: 17.5em,
        height: auto,
        margin: (x: 0.3em, top: 0.3em, bottom: 1em),
        fill: none,
    )

    yconf_math(doc)
}